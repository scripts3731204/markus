import requests
import json
from pprint import pprint
import urllib3
import sys

ubntsite = str(sys.argv[1])
ubnturl = str(sys.argv[2])
ubntport = sys.argv[3]

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
# set up connection parameters in a dictionary
gateway = {"ip": ubnturl, "port": ubntport}

# set REST API headers
headers = {"Accept": "application/json",
           "Content-Type": "application/json"}
# set URL parameters
loginUrl = 'api/login'
url = f"https://{gateway['ip']}:{gateway['port']}/{loginUrl}"
# set username and password
body = {
    "username": "admin",
    "password": "Zz5p-uJ3aE0lLbSh2!"
}
# Open a session for capturing cookies
session = requests.Session()
# login
response = session.post(url, headers=headers,
                        data=json.dumps(body), verify=False)

# parse response data into a Python object
api_data = response.json()
#print("/" * 50)
#pprint(api_data)
#print('Logged in!')
#print("/" * 50)

# Set up to get site name
getSitesUrl = 'api/self/sites'
url = f"https://{gateway['ip']}:{gateway['port']}/{getSitesUrl}"
response = session.get(url, headers=headers,
                       verify=False)
api_data = response.json()
#print("/" * 50)
#pprint(api_data)
#print("/" * 50)

# Parse out the resulting list of
responseList = api_data['data']
# pprint(responseList)
n = 'name'
for items in responseList:
#    if items.get('desc') == 'TT-Kinderhaus':
     if items.get('desc') == ubntsite:
        n = items.get('name')
# print(n)

getDevicesUrl = f"api/s/{n}/stat/device"
url = f"https://{gateway['ip']}:{gateway['port']}/{getDevicesUrl}"
response = session.get(url, headers=headers,
                       verify=False)
api_data = response.json()
responseList = api_data['data']
print('Export für CheckMK')
print(' ')
print(' ')
print('Hostname,IPv4 address')
for device in responseList:
    print(f"{device['name']},{device['ip']}")